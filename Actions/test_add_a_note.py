from appium import webdriver

def initiate_driver():
  desired_cap = {
    "deviceName": "4a8ddea30804",
    "platformName": "Android",
    "app": "C:\\Users\\Justyna Oszywa\\Documents\\Marcin\\Tester\\AlcoDroid Alcohol Tracker_v2.37.04.apk",
    "appPackage": "org.M.alcodroid",
    "appActivity": ".AlcoDroidAdSupportedActivity t6594",
    "automationName": "uiautomator2"
  }
  driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap)
  driver.implicitly_wait(30)

  return driver

def go_trough_consent(driver):
  driver.find_element_by_id("com.android.packageinstaller:id/permission_allow_button").click()
  driver.find_element_by_id("android:id/button1").click()


def go_to_add_drink(driver):
  driver.find_element_by_id("org.M.alcodroid:id/AddDrinkEntry").click()
  choose_preset_text = driver.find_element_by_id("android:id/action_bar_title").text

  assert choose_preset_text == "Choose a Preset", "We are not on a 'Choose a Preset' screen"

  driver.find_element_by_xpath("//android.widget.LinearLayout[@bounds='[0,496][1080,634]']").click()
  driver.find_element_by_id("org.M.alcodroid:id/ButtonAddDrink").click()


def test_add_a_note():

    driver = initiate_driver()
    go_trough_consent(driver)
    go_to_add_drink(driver)

    driver.find_element_by_id("org.M.alcodroid:id/ViewDrinkLog").click()
    view_edit_page = driver.find_element_by_id("android:id/action_bar_title").text

    assert view_edit_page == "View and Edit Drinks", "Not 'View and Edit Log' screen"

    driver.find_element_by_id("org.M.alcodroid:id/ButtonEditNote").click()
    driver.find_element_by_xpath("//android.widget.EditText[@bounds='[75,404][1005,500]']").send_keys("Na zdrowie!")
    driver.find_element_by_id("android:id/button1").click()

    comment = driver.find_element_by_id("org.M.alcodroid:id/ButtonEditNote").text
    print("Dodała się notatka " + "'" + comment + "'")
    assert comment == "Na zdrowie!", "TC 3.2a Notes don't match"

    driver.find_element_by_id("org.M.alcodroid:id/ButtonEditNote").click()
    driver.find_element_by_id("android:id/button2").click()

    print("Notatka po naciśnięciu cancel to " + comment)
    assert comment == "Na zdrowie!", "TC 3.2b Cancel did not work"

