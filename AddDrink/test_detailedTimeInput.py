from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction

def initiate_driver():
  desired_cap = {
    "deviceName": "dfa4edd1",
    "platformName": "Android",
    "app": "C:\\Users\\Damiano\\Desktop\\Appiumapp\\alko.apk",
    "appPackage": "org.M.alcodroid",
    "appActivity": ".AlcoDroidAdSupportedActivity t6594",
  }

  driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap)
  driver.implicitly_wait(30)

  return driver

def go_trough_consent(driver):
  driver.find_element_by_id("android:id/button1").click()

"""
Touch Actions
"""
def test_add_drink():
  driver = initiate_driver()
  go_trough_consent(driver)

  touch = TouchAction(driver)
  driver.find_element_by_id("org.M.alcodroid:id/AddDrinkEntry").click()
  touch.tap(x=586, y=478).perform()

  driver.find_element_by_id("org.M.alcodroid:id/TimeInputMethodButton").click()
  driver.find_element_by_id("org.M.alcodroid:id/WillPressButtonWhenFinished").click()
  driver.find_element_by_id("org.M.alcodroid:id/Time2Button").click()
  driver.find_element_by_xpath("//android.widget.EditText[@bounds='[563,951][755,1095]']").clear()
  driver.find_element_by_xpath("//android.widget.EditText[@bounds='[563,951][755,1095]']").send_keys("55")
  driver.press_keycode(4)
  driver.find_element_by_id("android:id/button1").click()
  driver.find_element_by_id("org.M.alcodroid:id/ButtonAddDrink").click()

  driver.find_element_by_id("org.M.alcodroid:id/ViewDrinkLog").click()
  preset_name = driver.find_element_by_id("org.M.alcodroid:id/name").text
  assert preset_name == "Light beer", "Nazwa presetu nie zgadza się"



