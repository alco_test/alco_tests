from appium import webdriver

def initiate_driver():
  desired_cap = {
    "deviceName": "4a8ddea30804",
    "platformName": "Android",
    "app": "C:\\Users\\Justyna Oszywa\\Documents\\Marcin\\Tester\\AlcoDroid Alcohol Tracker_v2.37.04.apk",
    "appPackage": "org.M.alcodroid",
    "appActivity": ".AlcoDroidAdSupportedActivity t6594",
    "automationName": "uiautomator2"
  }
  driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap)
  driver.implicitly_wait(30)

  return driver


def go_trough_consent(driver):
  driver.find_element_by_id("com.android.packageinstaller:id/permission_allow_button").click()
  driver.find_element_by_id("android:id/button1").click()


def go_to_add_drink(driver):
  driver.find_element_by_id("org.M.alcodroid:id/AddDrinkEntry").click()
  choose_preset_text = driver.find_element_by_id("android:id/action_bar_title").text

  assert choose_preset_text == "Choose a Preset", "We are not on a 'Choose a Preset' screen"


def test_add_simple_time_input():

  driver = initiate_driver()
  go_trough_consent(driver)
  go_to_add_drink(driver)

  drink_name = driver.find_element_by_xpath("//android.widget.TextView[@bounds='[0,496][984,577]']").text
  driver.find_element_by_xpath("//android.widget.LinearLayout[@bounds='[0,496][1080,634]']").click()
  name_in_preset_edition = driver.find_element_by_id("org.M.alcodroid:id/Name").text
  drink_added = driver.find_element_by_id("org.M.alcodroid:id/Name").text
  driver.find_element_by_id("org.M.alcodroid:id/ButtonAddDrink").click()


  assert drink_name == name_in_preset_edition, "'Wine' should be clicked"
  assert drink_name == drink_added, "'Wine' should be added"

