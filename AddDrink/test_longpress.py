from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction

def initiate_driver():
  desired_cap = {
    "deviceName": "dfa4edd1",
    "platformName": "Android",
    "app": "C:\\Users\\Damiano\\Desktop\\Appiumapp\\alko.apk",
    "appPackage": "org.M.alcodroid",
    "appActivity": ".AlcoDroidAdSupportedActivity t6594",
  }

  driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap)
  driver.implicitly_wait(30)

  return driver

def go_trough_consent(driver):
  driver.find_element_by_id("android:id/button1").click()

"""
Touch Actions
"""

def test_add_drink():
  driver = initiate_driver()
  go_trough_consent(driver)


  touch = TouchAction(driver)
  driver.find_element_by_id("org.M.alcodroid:id/AddDrinkEntry").click()
  touch.long_press(x=586, y=478).perform()
  driver.find_element_by_id("android:id/text1").click()
  driver.find_element_by_id("org.M.alcodroid:id/Name").clear()
  driver.find_element_by_id("org.M.alcodroid:id/Name").send_keys("Tyskie")
  driver.press_keycode(4)
  driver.find_element_by_id("org.M.alcodroid:id/ButtonSaveChanges").click()


  add_drink = driver.find_element_by_xpath("//android.widget.TextView[@text='Tyskie']").text
  assert add_drink == "Tyskie", "Nazwa presetu nie zgadza się"
