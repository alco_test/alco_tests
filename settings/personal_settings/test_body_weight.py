from appium import webdriver

def initiate_driver():
    desired_cap = {
        "platformName": "Android",
        "app": "C:\\Users\\Magda\\Desktop\\praca dyplomowa\\alco.apk",
        "deviceName": "1643d9bc7d44",
        "automationName": "uiautomator2",
        "appPackage": "org.M.alcodroid",
        "appActivity": ".AlcoDroidAdSupportedActivity t14115"
    }
    driver = webdriver.Remote( "http://localhost:4723/wd/hub",desired_cap )
    driver.implicitly_wait(30)
    return driver

def start_with_arrengments(driver):
    # Przeklikanie zgody i akceptacji regulaminu zeby dostać sie do ekranu głównego
    driver.find_element_by_id("com.android.packageinstaller:id/permission_allow_button").click()
    driver.find_element_by_id("android:id/button1").click()

def go_to_personalsettings(driver):
    # Przejście do okna "Personal Settings"
    # TC1 "Mobile menu (hamburger)"
    driver.find_element_by_class_name("android.widget.ImageButton").click()
    # TC2 "Personal Settings"
    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[3]/android.widget.RelativeLayout/android.widget.TextView").click()

def test_change_weight_ok():

    driver = initiate_driver()
    start_with_arrengments(driver)
    go_to_personalsettings(driver)

    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout").click()

    # TC4.2a Edycja "Body Weight"/Kliknięcie "OK"
    driver.find_element_by_id("android:id/edit").clear()
    driver.find_element_by_id("android:id/edit").send_keys("57")
    driver.find_element_by_id("android:id/button1").click()

    # Sprawdzenia czy zmiana nasatąpiła na poziomie "Personal Settings"
    weightInSettings = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.TextView[2]").text

    assert weightInSettings == "57 kilograms", "Failed, waga nie zgadza się z wrowadzoną"
    print("TC4.2a. Waga w Settings po zapisaniu zmiany wynosi:" + weightInSettings +" (powinna 57)")

def test_change_weight_anuluj():

    driver = initiate_driver()
    start_with_arrengments(driver)
    go_to_personalsettings(driver)

    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout").click()

    # TC4.2b Edycja "Body Weight"/Kliknięcie "Anuluj"
    driver.find_element_by_id("android:id/edit").clear()
    driver.find_element_by_id("android:id/edit").send_keys("59")
    driver.find_element_by_id("android:id/button2").click()

    # Sprawdzenie czy anuluj działa poprawnie (czy waga nie uległ zmianie)
    weightInSettings = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.TextView[2]").text
    assert weightInSettings == "70 kilograms", "Failed, waga nie zgadza się z oczekiwaną"
    print("TC4.2b. Waga w Settings po odrzuceniu zapisania zmiany(59) wynosi:" + weightInSettings + " (powinna70)")