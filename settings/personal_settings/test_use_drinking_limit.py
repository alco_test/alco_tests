from appium import webdriver

def initiate_driver():
    desired_cap = {
        "platformName": "Android",
        "app": "C:\\Users\\Magda\\Desktop\\praca dyplomowa\\alco.apk",
        "deviceName": "1643d9bc7d44",
        "automationName": "uiautomator2",
        "appPackage": "org.M.alcodroid",
        "appActivity": ".AlcoDroidAdSupportedActivity t14115"
    }
    driver = webdriver.Remote( "http://localhost:4723/wd/hub",desired_cap )
    driver.implicitly_wait(30)
    return driver

def start_with_arrengments(driver):
    # Przeklikanie zgody i akceptacji regulaminu zeby dostać sie do ekranu głównego
    driver.find_element_by_id("com.android.packageinstaller:id/permission_allow_button").click()
    driver.find_element_by_id("android:id/button1").click()

def go_to_personalsettings(driver):
    # Przejście do okna "Other Settings"
    # TC1 "Mobile menu (hamburger)"
    driver.find_element_by_class_name("android.widget.ImageButton").click()
    # TC2 "Other Settings"
    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[3]/android.widget.RelativeLayout/android.widget.TextView").click()

def test_activate_drinking_limit():
    driver = initiate_driver()
    start_with_arrengments(driver)
    go_to_personalsettings(driver)

    # naciśnecie "use drinking limit"
    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[3]").click()
    # Sprawdzenie czy pod "use drinking limits" zmienił sie tekst
    textUseDrinkingLimit = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[3]/android.widget.RelativeLayout/android.widget.TextView[2]").text
    assert textUseDrinkingLimit == "Set a limit for your consumption (compare in charts and statistics)", "Failed, tekst nie zgadza się z oczekiwanym"
    print("TC7.1a. Pod 'Use Drinkin Limit' widnieje tekst:" + textUseDrinkingLimit )

    # Sprawdzenie czy odhaczył sie checkbox
    useDrinkingLimitCheckbox = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.CheckBox").get_attribute('checked')
    assert useDrinkingLimitCheckbox , "Failed, checkbox nie jst odhaczony"
    print("TC7.1a. Checkbox jest odhaczony:" + useDrinkingLimitCheckbox )

    # Sprawdzenie czy pod "drinking limits" jest odszarzone
    VisibleDrinkingLimit = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[4]/android.widget.RelativeLayout").is_enabled()
    assert VisibleDrinkingLimit , "Failed,element powinien byc widoczny"
    print("TC7.1a. Tekst 'Drinking Limit' jest widoczny(odszarzony)" )

def test_desactivate_drinking_limit():
    driver = initiate_driver()
    start_with_arrengments(driver)
    go_to_personalsettings(driver)

    # naciśnecie "use drinking limit"
    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[3]").click()
    # TC7.1b "Use Drinking Limit"/Ponowne naciśnięcie "Use Drinking Limit"
    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[3]").click()
    # Sprawdzenie czy pod "use drinking limits" zmienił sie tekst
    textUseDrinkingLimit = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[3]/android.widget.RelativeLayout/android.widget.TextView[2]").text
    assert textUseDrinkingLimit == "Not using drinking limit", "Failed, tekst nie zgadza się z oczekiwanym"
    print("TC7.1b. Pod 'Use Drinkin Limit' widnieje tekst:" + textUseDrinkingLimit )


    # Sprawdzenie czy zniknał haczyk w checkbox
    useDrinkingLimitCheckbox = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.CheckBox").get_attribute('checked')


    if useDrinkingLimitCheckbox == 'false':
        useDrinkingLimitCheckbox = 0
    else:
        useDrinkingLimitCheckbox = 1

    useDrinkingLimitCheckbox = not useDrinkingLimitCheckbox

    assert useDrinkingLimitCheckbox , "Failed, checkbox nie jst odhaczony"
    print("TC7.1b. Checkbox jest odhaczony: false" )
    # Sprawdzenie czy  "drinking limits" jest niewidoczne
    VisibleDrinkingLimit = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[4]/android.widget.RelativeLayout").is_enabled()

    VisibleDrinkingLimit =  not VisibleDrinkingLimit

    assert VisibleDrinkingLimit , "Failed,element powinien byc niewidoczny"
    print("TC7.1b. Tekst 'Drinkig Limit' zaszarzony" )