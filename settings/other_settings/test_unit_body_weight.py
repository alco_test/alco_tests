from appium import webdriver

def initiate_driver():
    desired_cap = {
        "platformName": "Android",
        "app": "C:\\Users\\Magda\\Desktop\\praca dyplomowa\\alco.apk",
        "deviceName": "1643d9bc7d44",
        "automationName": "uiautomator2",
        "appPackage": "org.M.alcodroid",
        "appActivity": ".AlcoDroidAdSupportedActivity t14115"
    }
    driver = webdriver.Remote( "http://localhost:4723/wd/hub",desired_cap )
    driver.implicitly_wait(30)
    return driver

def start_with_arrengments(driver):
    # Przeklikanie zgody i akceptacji regulaminu zeby dostać sie do ekranu głównego
    driver.find_element_by_id("com.android.packageinstaller:id/permission_allow_button").click()
    driver.find_element_by_id("android:id/button1").click()

def go_to_othersettings(driver):
    # Przejście do okna "Other Settings"
    # TC1 "Mobile menu (hamburger)"
    driver.find_element_by_class_name("android.widget.ImageButton").click()
    # TC2 "Other Settings"
    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[4]/android.widget.RelativeLayout/android.widget.TextView").click()

def test_change_unit_pounds():
    driver = initiate_driver()
    start_with_arrengments(driver)
    go_to_othersettings(driver)

    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[1]").click()
    # TC13.1a Edycja "Body Weight Unit"/Naciśniecię radio buttona "pounds"
    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ListView/android.widget.CheckedTextView[2]").click()

    # Sprawdzenie czy zmiana działa poprawnie (czy w settingsach jest pounds)
    weightUnitInSettings = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.TextView[2]").text
    assert weightUnitInSettings == "pounds", "Failed, jednostka nie zgadza się z oczekiwaną ( powinno być pounds)"
    print("TC13.1a Jednoska po zmianie jest:" + weightUnitInSettings )

def test_change_unit_kilograms():
    driver = initiate_driver()
    start_with_arrengments(driver)
    go_to_othersettings(driver)

    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[1]").click()
    # TC13.1a Edycja "Body Weight Unit"/Naciśniecię radio buttona "pounds"
    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ListView/android.widget.CheckedTextView[2]").click()
    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[1]").click()
    # TC13.1b Edycja "Body Weight Unit"/Naciśniecię radio buttona "kilograms"
    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ListView/android.widget.CheckedTextView[1]").click()

    # Sprawdzenie czy zmiana działa poprawnie (czy w settingsach jest kilograms)
    weightUnitInSettings = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.TextView[2]").text
    assert weightUnitInSettings == "kilograms", "Failed, jednostka nie zgadza się z oczekiwaną (powinno być kilograms)"
    print("TC13.1b Jednoska po zmianie w  jest:" + weightUnitInSettings )

def test_change_unit_anuluj():
    driver = initiate_driver()
    driver.implicitly_wait(1)
    start_with_arrengments(driver)
    go_to_othersettings(driver)

    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout").click()
    # TC13.2 Edycja "Body Weight Unit"/Naciśnięcie "Anuluj"
    driver.find_element_by_id("android:id/button2").click()

    # Sprawdzenie czy zmiana działa poprawnie (czy w settingsach jest kilograms)
    weightUnitInSettings = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.TextView[2]").text
    assert weightUnitInSettings == "kilograms", "Failed, jednostka nie zgadza się z oczekiwaną (powinno być kilograms)"
    print("TC13.2 Jednoska po kliknieciu anuluj jest:" + weightUnitInSettings )
