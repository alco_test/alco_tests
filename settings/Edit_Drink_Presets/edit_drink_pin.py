from appium import webdriver

def initiate_driver():
  desired_cap = {
    "deviceName": "AQH7N18119004744",
    "platformName": "Android",
    "app": "C:\AlcoDroid Alcohol Tracker_v2.37.04_apkpure.com.apk",
    "appPackage": "org.M.alcodroid",
    "appActivity": "AlcoDroidAdSupportedActivity t6594",
  }
  
  driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap)
  driver.implicitly_wait(30)

  return driver

def go_trough_consent(driver):
  driver.find_element_by_id("com.android.packageinstaller:id/permission_allow_button").click()
  driver.find_element_by_id("android:id/button1").click()

def go_to_edit_drink_presets(driver):
  driver.find_element_by_xpath("//android.widget.ImageButton").click()
  driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.TextView").click()

def go_to_pin_to_main_screen(driver):
  driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[4]").click()
  driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.TextView[4]").click()
  driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ListView/android.widget.TextView[1]").click()

def test_add_simple_time_input():

  driver = initiate_driver()
  go_trough_consent(driver)
  go_to_edit_drink_presets(driver)
  go_to_pin_to_main_screen(driver)

  driver.press_keycode(4)
  correct = driver.find_element_by_id("org.M.alcodroid:id/AddRecentDrink1").text
  assert correct == "Add Generic c...50 ml, 13,0 %", "brak pinezki"
