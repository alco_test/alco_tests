from appium import webdriver

def initiate_driver():
  desired_cap = {
    "deviceName": "AQH7N18119004744",
    "platformName": "Android",
    "app": "C:\AlcoDroid Alcohol Tracker_v2.37.04_apkpure.com.apk",
    "appPackage": "org.M.alcodroid",
    "appActivity": "AlcoDroidAdSupportedActivity t6594",
  }
  driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap)
  driver.implicitly_wait(30)

  return driver

def go_trough_consent(driver):
  driver.find_element_by_id("com.android.packageinstaller:id/permission_allow_button").click()
  driver.find_element_by_id("android:id/button1").click()

def go_to_edit_drink_presets(driver):
  driver.find_element_by_xpath("//android.widget.ImageButton").click()

def edit_drink_preset(driver):
  driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.TextView").click()
  driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.TextView").click()
  driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.TextView[1]").click()
  driver.find_element_by_id("org.M.alcodroid:id/Name").clear()
  driver.find_element_by_id("org.M.alcodroid:id/Name").send_keys("Czerwone wino")
  driver.find_element_by_id("org.M.alcodroid:id/ButtonSaveChanges").click()

  assert drink_name == name_in_preset_edition, "'Wine' should be clicked"
  assert drink_name == name_changed, "'Wine' should be change"

def test_add_simple_time_input():

  driver = initiate_driver()
  go_trough_consent(driver)
  go_to_edit_drink_presets(driver)
  edit_drink_preset(driver)