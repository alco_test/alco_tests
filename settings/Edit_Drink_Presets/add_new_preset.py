from appium import webdriver

def initiate_driver():
  desired_cap = {
    "deviceName": "AQH7N18119004744",
    "platformName": "Android",
    "app": "C:\AlcoDroid Alcohol Tracker_v2.37.04_apkpure.com.apk",
    "appPackage": "org.M.alcodroid",
    "appActivity": "AlcoDroidAdSupportedActivity t6594",
  }

  driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap)
  driver.implicitly_wait(30)

  return driver

def go_trough_consent(driver):
  driver.find_element_by_id("com.android.packageinstaller:id/permission_allow_button").click()
  driver.find_element_by_id("android:id/button1").click()

def go_to_edit_drink_presets(driver):
  driver.find_element_by_xpath("//android.widget.ImageButton").click()

def add_new_preset(driver):
  driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[2]").click()
  driver.find_element_by_id("Więcej opcji").click()
  driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.TextView").click()

def change_values(driver):
  driver.find_element_by_id("org.M.alcodroid:id/Name").clear()
  driver.find_element_by_id("org.M.alcodroid:id/Name").send_keys("Whiskey")
  driver.find_element_by_id("org.M.alcodroid:id/Volume").clear()
  driver.find_element_by_id("org.M.alcodroid:id/Volume").send_keys("500 ml")
  driver.find_element_by_id("org.M.alcodroid:id/Alcohol").clear()
  driver.find_element_by_id("org.M.alcodroid:id/Alcohol").send_keys("40")

def save_rename(driver):
  driver.find_element_by_id("org.M.alcodroid:id/ButtonSaveChanges").click()

def test_add_simple_time_input():

  driver = initiate_driver()
  go_trough_consent(driver)
  go_to_edit_drink_presets(driver)
  add_new_preset(driver)
  change_values(driver)
  save_rename(driver)
